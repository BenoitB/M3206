## Exercice 1:

`git status` retourne l'état des fichiers ici elle nous retourne "nothing to commit"

Après avoir mis une ligne dans un fichier, la commande git status nous retourne " nothing to commit
but untracked files present"

Après avoir ajouté le fichier avec la commande git status nous retourne "change to be commited"
et on a le nouveau fichier en vert.

git commit -m "<Permet de publier un fichier>"

après avoir fait cette commande on a "nothing to commit"

on envoie grace a la commande git push origin master

maintenant la commande git status nous informe que notre "branch" est mis à jour

## Exercice 2:

La commande git log nous affiche tous les commit que nous avons effectué

Exercice 3:
lorsque l'on a modifié le fichier et que l'on fait git status , le fichier 
apparait comme modifié

après avoir tapé la commande git checkout -- TD1_Git.txt, la ligne à bien été supprimée
puis avec git status on redeviens à l'état dans lequel on étais avant d'enter la cinquième ligne

Exercice 4:
git status nous informe de la modification

après avoir entré la commande git reset HEAD TD1_Git.txt et l'autre, la ligne est bien supprimée

git status redeviens à l'état de la quatrième ligne


Exercice 5:

git revert HEAD~1..HEAD avec cette commande la quatrième ligne est bien supprimée


Exercice 7:
Le fichier ne contient pas encore la ligne ajoutée à partir de la BRANCH TEST
après avoir fait merge, le fichier contient les modifications de BRANCH TEST
git log nous afffiche le commit depuis le branch test

