### Find

Cette commande supprime les fichier ayant comme extension .txt et l'option ok demande la confirmation avant de supprimer les fichiers.

La commande cpio permet de copier des fichiers en prelevant une entrée standard en sortie standard.
La commande cp -R permet de copier tous les fichiers d'un répertoire.

Commande qui liste les premières lignes d'un fichier .txt d'un répertoire.
find /rep -name *.txt -exec head -10 {} \;
