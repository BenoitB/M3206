dpkg -l ssh >>/dev/null 2>&1

if [ $? -eq 0 ]; then
	echo "[...] ssh: est installé [...]"
else
	echo "[/!\] ssh: n'est pas installé [/!\]"
fi

ps aux|grep [s]shd >>/dev/null 2>&1 #ici on met le s de ssh entre crochet afin qu'il n'affiche pas le process grep ssh, il affichera donc si le process est lancé ou pas

if [ $? -eq 0 ]; then
	echo "[...] ssh:le service est lancé [...]"

else
	echo "[...] ssh: le service n'est pas lancé [...]"
	echo "[...] ssh: lancement du service [...]"
	/etc/init.d/ssh start
fi

