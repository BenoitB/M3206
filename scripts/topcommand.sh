
HISTFILE=~/.bash_history
set -o history # on charge le fichier history pour n'avoir aucune erreur  
history | awk '{print $2}' | uniq -c | sort -nr | head -$1
# on tape la commande history, ensuite on fait awk qui permet de ne garder que la deuxième colonne, on trie de manière unique les commandes qui se ressemblent et ont affiche le nombre de fois où elles apparaissent, sort -nr nous permet de trier par odre decroissant et enfin head -$1 nous permet de trier selon le nombre entré
