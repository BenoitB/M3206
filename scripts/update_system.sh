
if [ "$UID" -eq "0" ];
then
	echo "[...] update database[...]"
apt-get update >>/dev/null 2>&1 # 2>&1 permet de renvoyer la sortie erreur et la sortie standard dans le trou noir /dev/null
	echo "[...] upgrade system [...]"
apt-get upgrade >>/dev/null 2>&1

else
	echo "[/!\] Vous devez être super-utilisateur[/!\]"
fi

