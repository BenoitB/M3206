	echo "[...] Checking internet connection [...]"
ping -c2 8.8.8.8 >/dev/null  #permet d'envoyer la commande ping sur le fichier /dev/null et on envoie que deux requetes
if [ $? -eq 0 ]; then #si la commande renvoie 0, signife qu'elle a fonctionnée correctement, alors le ping marche et on est connecte a internet
	echo "[...] Internet access OK [...]"
else
	echo "[/!\] Not connected to Internet [/!\]"
fi

