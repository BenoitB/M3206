dpkg -l git >>/dev/null 2>&1 #on verifie que le paquet existe et on renvoie les sortie sur le fichier /dev/null
if [ $? -eq 0 ]; then #on verifie que la commande renvoie 0 et si c'est le cas c'est qu'il est installé sinon non
	echo "[...] git: installé [...]"
else
	echo "[/!\] git: pas installé [/!\] lancer la commande 'apt-get install git'"

fi

dpkg -l tmux >>/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[...] tmux: installé [...]"
else
	echo "[/!\] tmux: pas installé [/!\] lancer la commande 'apt-get install tmux'"
fi

dpkg -l vim >>/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[...] vim: installé [...]"
else
	echo "[/!\] vim: pas installé [/!\] lancer la commande 'apt-get install vim'"
fi

dpkg -l htop >>/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[...] htop: installé [...]"
else
	echo "[/!\] htop: pas installé [/!\] lancer la commande 'apt-get install htop'"
fi
