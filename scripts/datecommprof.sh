#!/bin/bash

f="first"
l="last"

if [ -z $1 ] || [ -z $2 ] || [ -z $3 ]
then
	echo "The command takes 3 arguments." #il doit y a voir dans sa commande 3 arguments
echo "usage: ./datecomm.sh [first|last] command file"
exit 1
fi


nbfois=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ';' -f2 | wc -l) 
#on regarde le nombre de fois que la commande passée en paramètre apparait
#on utilise wc -l pour compter le nombre de ligne de la sortie, donc si on a 0 c'est quelle n'apparait jamais
if [ $nbfois -eq 0 ] 		#on regarde si on la commande apparait dans le fichier, si elle apparait pas, on dit à l'utilisateur que la commande n'apparait pas dans l'historique 
then
echo "The history does not contain "$2" command"
exit 1
fi

if [ $1 = $f ]

then

temps=$(cat $3 | awk '{print $2}' | grep $2| cut -d ':' -f1 | head -1 )
fi

if [ $1 = $l ]
then

temps=$(cat $3 | awk '{print $2}' | grep $2| cut -d ':' -f1 | tail -1 )
fi 

if [ $1 != $f ] && [ $1 != $l ] #on verifie si le parametre est correct
then
echo " First parameter is 'last' of 'first' "
echo " usage : ./datecommprof.sh [last|first] [command] [file]"
fi

if [ ! -e $3 ] #on verifie si le fichier existe
then
echo " Le fichier "$3" n'existe pas " 
echo " usage : ./datecommprof.sh [last|first] [command] [file]"
fi




#La variable temps va nous permettre d'afficher le timestamp
date -d @$temps
#on fait deux boucles if afin de prendre le cas ou l'on entre first et last
