## Exercice 01:

Pour descendre d'une demi-page on utilise ctrl+d ensuite on insère grâce à la commande O juste au dessus de la ligne "this" et on appuie sur entrer pour faire un saut de ligne.

Puis on tape ctrl+u pour remonter en haut du texte, enfin on tape :6 pour aller a la ligne 6, on copie le texte grâce à Y, on colle à la ligne suivante grâce à p et enfin on modifie. Pour finir en enregistre et on quitte grâce à :x


## Exercice 02:

On recherche dans tout le fichier grâce à 
````bash
:g/Sed/s//XXX/g et :g/sed/s//xxx/g.
```

## Exercice 03:

On se place à la deuxième ligne, puis on tape 3dd afin de supprimer 3 lignes, on fait $ pour aller à la fin de la ligne et on supprime tout jusqu'au a avec la commande x et on modifie.
Pour Hello on supprime les caractère après le a avec la commande x.


## Exercice 04:

On rajoute un V, puis on tape :%s/V/i/g afin de remplacer les 100 i.
Ou on peut faire 99 puis r puis ii.
